#pragma once


#include <gtk/gtk.h>

#include "etechapp.h"


#define __unused 	__attribute__((unused))
#define __used 		__attribute__((used))


#define ETECH_APP_WINDOW_TYPE (etech_app_window_get_type())
#define ETECH_APP_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), \
									ETECH_APP_WINDOW_TYPE, ETechAppWindow))


typedef struct _ETechAppWindow         ETechAppWindow;
typedef struct _ETechAppWindowClass    ETechAppWindowClass;


GType
etech_app_window_get_type(void);

// Note : passage d'un pointeur vers la fenêtre principale.
ETechAppWindow*
etech_app_window_new(ETechApp *app);

