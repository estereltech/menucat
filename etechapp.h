/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#pragma once

#include <gtk/gtk.h>

#include <locale.h>

#include "config.h"

G_BEGIN_DECLS

#define ETECH_APP_TYPE (etech_app_get_type())
#define ETECH_APP(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), \
													ETECH_APP_TYPE, ETechApp))


typedef struct _ETechApp       		ETechApp;
typedef struct _ETechAppClass  		ETechAppClass;


GType     	etech_app_get_type    	(void);
ETechApp*	etech_app_new         	(void);

// Accesseurs.
GdkPixbuf*
etech_app_get_logo(ETechApp *app);
GdkPixbuf*
etech_app_get_icon(ETechApp *app);

G_END_DECLS
