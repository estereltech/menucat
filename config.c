#include "config.h"


//// Privé.


#define ETECH_CONFIG_DIR				"Estérel-Tech"
#define ETECH_CONF_DIR_PERMISSIONS		0755
#define ETECH_APP_CONF_FILE_NAME  		"MenuCat.conf"
#define CONF_GROUP_MENU					"menu"
#define CONF_KEY_COORDS_LEFT			"coords_left"
#define CONF_KEY_COORDS_TOP				"coords_top"
#define CONF_KEY_TERM_CMD				"term_cmd"

// Configuration par défaut au premier lancement de MenuCat.
#define CONF_DATA "#\n"										\
	"#Position du menu sur l'écran :\n"						\
	"#\n"													\
	"[menu]\n"												\
	"coords_left=4\n"										\
	"coords_top=100\n"										\
	"#Terminal à utiliser - ex. 'gnome-terminal --' "		\
	"'gnome-terminal --' ou 'x-terminal-emulator -e'.\n" 	\
	"term_cmd="

struct stconfig
{
    GKeyFile 	*key_file;
    gchar 		*key_file_path;
    gboolean	is_loaded;
};


//// Public.


struct stconfig*
config_new()
{
    struct stconfig *cfg = NULL;
    cfg = (struct stconfig*)g_malloc(sizeof(struct stconfig));
    if ( cfg != NULL ) {
        cfg->key_file 		= NULL;
        cfg->key_file_path 	= NULL;
        cfg->is_loaded		= FALSE;
    }

    cfg->key_file = g_key_file_new();
    if ( cfg->key_file == NULL ) {
        g_free(cfg);
        cfg = NULL;
    }

    return cfg;
}

void
config_free(struct stconfig *cfg)
{
    if ( cfg == NULL ) {
        return;
    }

    if ( cfg->key_file_path != NULL ) {
        g_free(cfg->key_file_path);
    }
    cfg->key_file_path = NULL;

    if ( cfg->key_file != NULL ) {
        g_key_file_free(cfg->key_file);
    }
    cfg->key_file = NULL;

    g_free(cfg);
    cfg = NULL;
}

/*
 * Chargement du fichier de configuration.
 * S'il n'existe pas déjà, le fichier est créé dans le répertoire dédié, ici :
 * '/home/$USER/.config/ETECH_CONFIG_DIR/ETECH_APP_CONF_FILE_NAME'.
 */
gboolean
config_load(struct stconfig *cfg)
{
    GError *error 	= NULL;
    gchar *dir_path = NULL;

    /*
     * Répertoire de configuration de l'utilisateur en cours, normalement
     * '/home/$USER/.config'.
     */
    const gchar *user_cfg_dir = NULL;
    user_cfg_dir = g_get_user_config_dir();
    if ( ! user_cfg_dir ) {
        return FALSE;
    }

    /*
     * Création d'un répertoire 'ETECH_CONFIG_DIR' dans le répertoire de
     * configuration de l'utilisateur s'il n'existe pas déjà, par exemple :
     * '/home/$USER/.config/ETECH_CONFIG_DIR'
     */
    dir_path = g_strdup_printf("%s%s%s",
                               user_cfg_dir,
                               G_DIR_SEPARATOR_S,
                               ETECH_CONFIG_DIR);
    if ( ! dir_path ) {
        return FALSE;
    }

    if ( ! g_file_test(dir_path, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR) ) {

        if ( g_mkdir(dir_path, ETECH_CONF_DIR_PERMISSIONS) != 0 ) {
            g_print("Erreur à la création du répertoire %s\n", dir_path);
            g_free(dir_path);

            return FALSE;
        }
    }

    /*
     * Création d'un fichier de configuration dans ce répertoire, par exemple :
     * '/home/$USER/.config/ETECH_CONFIG_DIR/ETECH_APP_CONF_FILE_NAME'.
     */
    if ( ! g_key_file_load_from_data(cfg->key_file,
						CONF_DATA,
						-1,
						G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS,
						&error) ) {

        if ( error ) {
            g_print("%s\n", error->message);
            g_error_free(error);
            error = NULL;
            g_free(dir_path);

            return FALSE;
        }
    }

    // Chemin vers le fichier.
    cfg->key_file_path = g_strdup_printf("%s%s%s",
                                         dir_path,
                                         G_DIR_SEPARATOR_S,
                                         ETECH_APP_CONF_FILE_NAME);
    if ( cfg->key_file_path == NULL ) {
        g_print("%s\n", "key_file_path = NULL");
        g_free(dir_path);

        return FALSE;
    }

    // Ne créer le fichier que s'il n'existe pas déjà.
    if ( ! g_file_test(cfg->key_file_path,
                       G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR) ) {

        if ( ! g_key_file_save_to_file(cfg->key_file,
                                       cfg->key_file_path,
                                       &error) ) {
            if ( error ) {
                g_print("%s\n", error->message);
                g_error_free(error);
                error = NULL;
                g_free(dir_path);

                return FALSE;
            }
        }
    }

    // Charger le fichier de configuration.
    g_key_file_load_from_file(cfg->key_file,
                              cfg->key_file_path,
                              G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR,
                              &error);
    if ( error ) {
        g_print("%s\n", error->message);
        g_error_free(error);
        error = NULL;
        g_free(dir_path);

        return FALSE;
    }

    g_free(dir_path);
    cfg->is_loaded = TRUE;

    return TRUE;
}

gboolean
config_is_loaded(struct stconfig *cfg)
{
	return cfg->is_loaded;
}

// Les coordonnées seront ileft et itop.
gboolean
config_set_menu_left(struct stconfig *cfg, gint ileft)
{
	GError *error = NULL;

	g_key_file_set_integer(cfg->key_file,
                           CONF_GROUP_MENU,
                           CONF_KEY_COORDS_LEFT,
                           ileft);

	if ( ! g_key_file_save_to_file(cfg->key_file,
                                   cfg->key_file_path,
                                   &error) ) {
		if ( error ) {
			g_print("%s\n", error->message);
			g_error_free(error);
	        error = NULL;
	    }

        return FALSE;
    }

    return TRUE;
}

gboolean
config_set_menu_top(struct stconfig *cfg, gint itop)
{
	GError *error = NULL;

	g_key_file_set_integer(cfg->key_file,
                           CONF_GROUP_MENU,
                           CONF_KEY_COORDS_TOP,
                           itop);

	if ( ! g_key_file_save_to_file(cfg->key_file,
                                   cfg->key_file_path,
                                   &error) ) {

        if ( error ) {
            g_print("%s\n", error->message);
            g_error_free(error);
            error = NULL;
        }

        return FALSE;
    }

    return TRUE;
}

gboolean
config_set_menu_coords(struct stconfig *cfg, gint ileft, gint itop)
{
    GError *error = NULL;

    g_key_file_set_integer(cfg->key_file,
                           CONF_GROUP_MENU,
                           CONF_KEY_COORDS_LEFT,
                           ileft);
    g_key_file_set_integer(cfg->key_file,
                           CONF_GROUP_MENU,
                           CONF_KEY_COORDS_TOP,
                           itop);

    if ( ! g_key_file_save_to_file(cfg->key_file,
                                   cfg->key_file_path,
                                   &error) ) {

        if ( error ) {
            g_print("%s\n", error->message);
            g_error_free(error);
            error = NULL;
        }

        return FALSE;
    }

    return TRUE;
}

gboolean
config_get_menu_coords(struct stconfig *cfg,
						gint *ileft,
						gint *itop)
{
    GError *error = NULL;

    gint _left = g_key_file_get_integer(cfg->key_file,
                                        CONF_GROUP_MENU,
                                        CONF_KEY_COORDS_LEFT,
                                        &error);
    if ( error ) {
        g_print("%s\n", error->message);
        g_error_free(error);
        error = NULL;

        return FALSE;
    }

    gint _top = g_key_file_get_integer(cfg->key_file,
                                       CONF_GROUP_MENU,
                                       CONF_KEY_COORDS_TOP,
                                       &error);
    if ( error ) {
        g_print("%s\n", error->message);
        g_error_free(error);
        error = NULL;

        return FALSE;
    }

    *ileft = _left;
    *itop = _top;

    return TRUE;
}

gboolean
config_set_terminal_command(struct stconfig *cfg, const gchar *tcmd)
{
	GError *error = NULL;

    g_key_file_set_string(cfg->key_file,
                           CONF_GROUP_MENU,
                           CONF_KEY_TERM_CMD,
                           tcmd);

	if ( ! g_key_file_save_to_file(cfg->key_file,
                                   cfg->key_file_path,
                                   &error) ) {

        if ( error ) {
            g_print("%s\n", error->message);
            g_error_free(error);
            error = NULL;
        }

        return FALSE;
    }

    return TRUE;
}

gchar*
config_get_terminal_command(struct stconfig *cfg)
{
	GError *error = NULL;

	gchar *_tcmd = g_key_file_get_string(cfg->key_file,
                                       CONF_GROUP_MENU,
                                       CONF_KEY_TERM_CMD,
                                       &error);
    if ( error ) {
        g_print("%s\n", error->message);
        g_error_free(error);
        error = NULL;

        return NULL;
    }

	// g_print("_tcmd = %s\n", _tcmd);

    return _tcmd;
}
