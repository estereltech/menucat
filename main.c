/*

	Projet		: MenuCat - menu d'applications pour système Linux.
	Auteur		: © Philippe Maréchal
	Date		: 29 décembre 2018
	Version		: 1.2

	Notes		:
	Ce menu des applications est conforme à Freedesktop.org, quant à l'
	interopérabilité des environnements graphiques sur systèmes Linux / Unix.
	Note : l'utilisation de :
	status = g_application_run(app, argc, argv);
	g_object_unref(app);

	return status;
	permet d'activer les gestionnaires des signaux 'dispose' et 'finalize' de l'
	application.
	L'application n'est pas internationalisée en l'état.

	Révisions	:
	29 décembre 2018 > 1ère version complètement fonctionnelle.
	29 décembre 2018 > 'gtk_image_set_pixel_size' pour les menus.
	30 décembre 2018 > les menus sont créés au lancement de l'application.
	31 décembre 2018 > barre d'état spécifiant le nombre d'applications.
	06 janvier 2019 > 'find_path_of_exe' remplace 'find_path_of'.
	08 janvier 2019 > module 'img_menu_item[h,c]'.
	09 janvier 2019 > modifications mineures & tests sur Bunsen Labs Linux.
	03 février 2019 > remplacement de 'execve' par 'g_spawn_command_line_async'
	=> simplification + plus besoin de la fonction 'find_path_of_exe'.
	15 avril 2019 > Menu avec fenêtre invisible !
	16 avril 2019 > Extension du menu avec 'Configuration', 'À propos...',
	séparateurs et gestionnaires de signaux et gestion des catégories
	inutilisées (menus vides).
	17 avril 2019 > Configuration pour les coordonnées + améliorations et
	version 1.2.
	18 avril 2019 > Améliorations, révisions des commentaires, gestion des
	objets en état 'flottant' et contrôle de 'g_list_free_full'.
	19 avril 2019 > correction > config_free(priv->cfg).
	20 avril 2019 > Configuration créée par l'application et enregistrement de
	l'application sur D-BUS pour une instance unique + options de la ligne de
	commandes.
	21 avril 2019 > tests et contrôles d'eventuels processus 'zombies'.
	02 mai 2019 > les applications lancées par le 'Terminal' ne sont pas prises
	en charge par MenuCat ou bien seulement pour Debian (/etc/alternatives/
	x-terminal-emulator). Autres modifications dans 'etechappwin.c'.
	05 mai 2019 > corrections de bogues quant à l'affichage du menu et du
	dialogue 'À propos' dans l'environnement de bureau 'openbox'.
	06 mai 2019 > la configuration est du ressort de la fenêtre principale.
	08 mai 2019 > testé sur la distribution MX Linux + modifications mineures.
	11 mai 2019 > meilleurs commentaires.

*/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */


#include <gtk/gtk.h>

#include <stdlib.h>

#include "etechapp.h"
#include "package_version.h"


#define __unused	__attribute__((unused))
#define __used		__attribute__((used))


// Option de type G_OPTION_ARG_CALLBACK.
static gboolean
_print_version_and_exit(__unused  	const gchar *name,
			 __unused const gchar 	*value,
			 __unused gpointer     	user_data,
			 __unused GError     	**error)
{
	g_print("Application : %s\nversion : %s\n", PACKAGE_NAME, PACKAGE_VERSION);

	exit(EXIT_SUCCESS);
	return TRUE;
}

static const GOptionEntry options[] = {
	{ "version", 'v', G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK,
		_print_version_and_exit, "Affiche la version de l'application", NULL },
	{ NULL }
};

static gboolean
_parse_options(int argc, char **argv)
{
	GOptionContext *context;
	GError *error = NULL;
	gboolean success;

	context = g_option_context_new("- Menu d'applications de bureau conforme à "
													"Freedesktop.org.");
	g_option_context_add_main_entries(context, options, NULL);

	/* This initialises GTK during parsing. */
	g_option_context_add_group(context, gtk_get_option_group(FALSE));

	success = g_option_context_parse(context, &argc, &argv, &error);
	g_option_context_free(context);

	if ( ! success ) {
		gchar *help;

		help = g_strdup_printf("Run '%s --help' to see a full "
								"list of available command line "
								"options", argv[0]);
		g_printerr("%s\n%s\n", error->message, help);

		g_clear_error(&error);
		g_free(help);

		return FALSE;
	}

	return TRUE;
}


int
main(int argc, char **argv)
{
	gint status 		= EXIT_FAILURE;
	GApplication *app 	= NULL;
	GError *error 		= NULL;
	gboolean reg 		= FALSE;

	/* Setup locale/gettext */
	setlocale(LC_ALL, "");

	if ( ! _parse_options(argc, argv) ) {
		return status;
	}

	app = G_APPLICATION(etech_app_new());

	/*
	 * https://honk.sigxcpu.org/con/GTK__and_the_application_id.html
	 * Pour que l'application soit correctement affichée avec son icône sous
	 * Wayland.
	 */
	g_set_prgname(APPLICATION_ID);

	// Instance unique de l'application.
    reg = g_application_register(app, NULL, &error);
    if ( reg == FALSE || error != NULL ) {
        g_warning("Unable to register GApplication: %s", error->message);
        g_error_free(error);
        error = NULL;

        return status;
    }

    if ( g_application_get_is_remote(app) ) {
        g_object_unref(app);

        return status;
    }

	// Boucle de l'application.
    status = g_application_run(app, argc, argv);
	g_object_unref(app);

	return status;
}

