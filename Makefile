#
# Makefile simple PhM 180724
# Révision 210118
#

# Shell par défaut.
SHELL=/bin/sh

# Compilateur.
CC?=cc

# Nom de l'exécutable en sortie.
APP_NAME=MenuCat

# Les fichiers source C à compiler.
SRCS=						\
		main.c 				\
		etechapp.c 			\
		etechappwin.c 		\
		options.c 			\
		config.c 			\
		etech_utils.c


# Pour les ressources à compiler avec l'application.
GRES_C=resource.c
GRES_XML=gresource.xml
GRES_DIR=res

# Ajout de resource.c aux fichiers source C.
SRCS+=$(GRES_C)

#
# Nombre de coeurs de processeur.
#
CORES ?= $(word 4,$(shell grep 'cpu cores' /proc/cpuinfo | uniq))
MAKEFLAGS += --jobs=$(CORES)

#
# La taille de l'exécutable est le 5 ème mot de la ligne ls.
#
define lstarget
echo Taille de $1 : $(word 5,$(shell ls -l $1)) octets.
endef

# Fichier de sortie.
OUT=-o $(APP_NAME)

# Optimisé, sans information de débogage.
CRELEASE=-DNDEBUG -Ofast -s

# Débogage.
CDEBUG=-Og -g

# Options de compilation : avertissements et erreurs, C11.
CFLAGS=-march=native -Wall -Wextra -Werror -std=gnu11 -D_GNU_SOURCE

# Liaison aux bibliothèques propres à GTK+ (version 3.0).
CPPLAGS=`pkg-config --cflags gtk+-3.0`
LDFLAGS=`pkg-config --libs gtk+-3.0`


# Cibles ne correspondant à aucun fichier.
.PHONY: all release debug _rel _dbg clean

# Cible par défaut.
.DEFAULT_GOAL=release


# Cibles.
release: $(GRES_C) _rel
	@echo "Compilation avec $(CORES) coeur(s)."
	@$(call lstarget,$(APP_NAME))

debug: $(GRES_C) _dbg
	@echo "Compilation avec $(CORES) coeur(s)."
	@$(call lstarget,$(APP_NAME))


# Compilation de l'exécutable.
_rel: $(SRCS)
	$(CC) $(CFLAGS) $(CRELEASE) $(CFLAGS) $(OUT) $^ $(CPPLAGS) $(LDFLAGS)

_dbg: $(SRCS)
	$(CC) $(CFLAGS) $(CDEBUG) $(CFLAGS) $(OUT) $^ $(CPPLAGS) $(LDFLAGS)

# Compilation des ressources.
$(GRES_C): $(GRES_XML) $(GRES_DIR)
	glib-compile-resources --target=$@					\
							--generate-source $<  		\
							--sourcedir=$(word 2,$^)

# Nettoyage.
clean:
	$(RM) *.h~
	$(RM) *.c~
	$(RM) Makefile~
	$(RM) $(GRES_C)

clobber: clean
	$(RM) $(APP_NAME)
