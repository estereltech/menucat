#pragma once

#define PACKAGE_NAME "MenuCat"
#define PACKAGE_VERSION "1.2.0.52 (10 juillet 2019)"
#define COPYRIGHT_AUTHOR "© 2019 Philippe Maréchal"
#define APPLICATION_ID "com.etech.MenuCat"
