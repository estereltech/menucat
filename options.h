/*

	Projet		: Exemple de widget dialogue d'options de configuration.
	Auteur		: © Philippe Maréchal
	Date		: 11 janvier 2019
	Version		:

	Notes		:
	Hérité de GtkDialog.
	
	A faire		: 

	Révisions	:
	13 janvier 2019 > la configuration est un membre privé de la fenêtre
	principale qui est passé en argument dans le constructeur du dialogue.
	14 janvier 2019 > passage d'un pointeur vers la fenêtre principale de
	l'application dans le constructeur du dialogue.
	15 janvier 2019 > dimensions de l'écran : 'priv->screen_rectangle'.
	25 mars 2019 > ajout d'un cadre 'Préférences' au dialogue et meilleur
	placement des Widgets dans la grille.
	29 mars 2019 > marques indiquant les positions centrées de la fenêtre +
	fonction '_get_center'.
	17 avril 2019 > Adapté et simplifié pour l'application 'MenuCat'.
	06 juin 2019 > retiré GtkGrid et style de la boîte verticale.

*/


#pragma once

#include <gtk/gtk.h>

#include "config.h"


#define __unused 	__attribute__((unused))
#define __used 		__attribute__((used))


#define ETECH_OPTIONS_DIALOG_TYPE (etech_options_dialog_get_type())
#define ETECH_OPTIONS_DIALOG(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), \
							ETECH_OPTIONS_DIALOG_TYPE, ETechOptionsDialog))

typedef struct _ETechOptionsDialog         ETechOptionsDialog;
typedef struct _ETechOptionsDialogClass    ETechOptionsDialogClass;

GType        	etech_options_dialog_get_type(void);
GtkWidget*		etech_options_dialog_new(Config cfg);

