/*

	Projet    : Configuration pour une application GTK+.
	Auteur    : © Philippe Maréchal
	Date      : 12 janvier 2019
	Version   :

	Notes     :
	Le module ne dérive pas de l'interface GObject bien qu'il utilise GLib.

	Révisions :
	13 janvier 2019 > structure 'stconfig' et permissions du fichier de
	configuration : CONF_FILE_PERMISSIONS + 'cfg->key_file' est créé dans le
	constructeur.
	16 janvier 2019 > ETECH_CONF_DIR_PERMISSIONS (0700).
	25 mars 2019 > possibilité de modifier les coordonnées séparément.
	17 avril 2019 > adapté à l'application 'MenuCat' et correction d'un bogue
	dans config_load(Config).

*/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */


#pragma once

#include <glib.h>
#include <glib/gstdio.h>


#define __unused 	__attribute__((unused))
#define __used 		__attribute__((used))


typedef struct stconfig *Config;


// Constructeur / destructeur.
Config
config_new(void);
void
config_free(Config);

// Chargement ou création du fichier.
gboolean
config_load(Config);
gboolean
config_is_loaded(Config cfg);

// Accesseurs.
gboolean
config_set_menu_left(Config cfg, gint ileft);
gboolean
config_set_menu_top(Config cfg, gint itop);
gboolean
config_set_menu_coords(Config, gint ileft, gint itop);
gboolean
config_get_menu_coords(Config, gint *ileft, gint *itop);
gboolean
config_set_terminal_command(Config cfg, const gchar *tcmd);
gchar*
config_get_terminal_command(Config cfg);
