**MenuCat - menu d'applications par catégories pour système Linux.**  
*Langage C - Interface graphique utilisateur GTK+ 3 >= 3.20*   

Ce menu des applications est conforme à Freedesktop.org, quant à l'
interopérabilité des environnements graphiques sur systèmes Linux / Unix.
Les applications sont proposées par catégories.
Les 10 principales catégories utilisées sont :
Accessoires, Bureautique, Éducation, Infographie, Internet - Réseau,
Jeux, Multimédia, Paramètres, Programmation, Système.   

À l'origine, j'ai créé cette application pour un ordinateur portable ayant pour système d'exploitation la distribution GNU/Linux Debian "testing" avec l'environnement de Bureau OpenBox.  
Les autres environnements de bureau, comme les plus connus, Xfce, Gnome, Mate, KDE etc. ont leur propre menu d'applications. MenuCat est donc destiné à des environnements de bureau minimalistes et se montre très efficace avec un raccourci clavier.  

**Captures d'écran**  
Le rendu dépend du thème GTK 3.x utilisé.  

![Menu principal](screenshots/mc_main.png)  

![Menu secondaire](screenshots/mc_sub.png)  

![Configuration](screenshots/mc_conf.png)  

![Dialogue 'À propos'](screenshots/mc_about.png)  

**Notes :**  
La configuration simple permet d'indiquer les coordonnées d'affichage du menu
sur l'écran ainsi que la commande permettant d'exécuter des applications
nécessitant le terminal. Pour les distributions Debian et dérivées MenuCat
utilise /etc/alternatives/x-terminal-emulator (émulateur de terminal par
défaut). Pour les autres distributions, par exemple celles utilisant l'
environnement de bureau Gnome, la commande est : 'gnome-terminal --'; pour
d'autres émulateurs de terminal, se référer à leurs manuels.
Au premier lancement de MenuCat les coordonnées par défaut sont x=4, y=100, ce
qui place le menu à 4 pixels du bord gauche de l'écran et à 100 pixels du bord
supérieur.  
Le fichier de configuration menucat.conf est créé dans le répertoire
/home/$USER/.config/Estérel-Tech/.  
Un dialogue 'À propos' est proposé par le menu.  
L'application MenuCat n'est pas internationalisée en l'état.  

**Tests effectués sur les versions récentes des distributions suivantes :**  
Debian (10 & 11) stable et testing  

Machines virtuelles :  
Fedora  
Arch Linux  
Mint  
Ubuntu (Wayland n'est pas encore au point, choisir Xorg)  
Xubuntu  
Mageia  
Calculate Linux  
BunsenLabs  
CrunchBang++  

**Compilation et utilisation**  
Il est nécessaire d'avoir GCC (GNU compiler collection), pkg-config ainsi que les outils de développement (build-essential, sous Debian) pour pouvoir compiler le programme et obtenir un fichier exécutable.
L'application est liée aux bibliothèques partagées GTK+ 3 comme cela est indiqué dans le Makefile :  
*# Liaison aux bibliothèques propres à GTK+ (version 3.0).*  
*CPPLAGS=`` `pkg-config --cflags gtk+-3.0` ``*  
*LDFLAGS=`` `pkg-config --libs gtk+-3.0` ``*  

Ouvrir un terminal dans le répertoire du programme.  

Pour compiler en mode optimisé : make  
Pour compiler en mode de débogage : make debug  
Pour exécuter le programme : ./MenuCat  


