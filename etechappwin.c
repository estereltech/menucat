#include "etechappwin.h"
#include "etech_utils.h"
#include "config.h"
#include "options.h"
#include "package_version.h"

#include <gio/gdesktopappinfo.h>

#include <string.h>

//// Privé.


#define IMG_SIZE 	16
#define CMD_LEN		256

// Les 10 principales catégories.
typedef enum {
	CAT_UTILITY 	= 0,
	CAT_OFFICE 		= 1,
	CAT_EDUCATION 	= 2,
	CAT_GRAPHICS 	= 3,
	CAT_NETWORK 	= 4,
	CAT_GAME 		= 5,
	CAT_AUDIOVIDEO 	= 6,
	CAT_SETTINGS 	= 7,
	CAT_DEVELOPMENT = 8,
	CAT_SYSTEM 		= 9,
	NB_CAT			// Nombre de catégories (10).
} en_category;

char *CATEGORY[NB_CAT] = {
	"Accessoires",
	"Bureautique",
	"Éducation",
	"Infographie",
	"Internet - Réseau",
	"Jeux",
	"Multimédia",
	"Paramètres",
	"Programmation",
	"Système"
};

/*
 * Structure privée de la fenêtre principale.
 */
typedef struct _ETechAppWindowPrivate ETechAppWindowPrivate;
struct _ETechAppWindowPrivate {

	// L'application.
	ETechApp *app;

	// Liste chaînée des applications de bureau du système.
	GList *apps;

	// Menu principal : catégories + 'Configuration' & 'À propos...'
	GtkWidget *cats_menu;

	// Tableau des widgets GtkMenu (catégories) utilisés.
	GtkWidget *menus[NB_CAT];

	/*
	 * Drapeau empêchant le fenêtre d'être fermée et de quitter l'application.
	 * Utilisé pour les dialogues 'À propos...' et celui de configuration.
	 */
	gboolean dialog_flag;
	gboolean popped_up;

	// Configuration et coordonnées d'affichage sur l'écran.
	Config cfg;
	gint left_coord;
	gint top_coord;
	gchar *term_cmd;
};

/*
 * Structure de la fenêtre.
 * Héritage de 'GtkApplicationWindow'.
 */
struct _ETechAppWindow {
    GtkApplicationWindow parent;
};

/*
 * Structure de la classe de fenêtre.
 * Héritage de 'GtkApplicationWindowClass'.
 */
struct _ETechAppWindowClass {
    GtkApplicationWindowClass parent_class;
};

G_DEFINE_TYPE_WITH_PRIVATE(ETechAppWindow,
							etech_app_window,
							GTK_TYPE_APPLICATION_WINDOW);

// Fonction de comparaison (pour le tri) en UTF-8.
static gint __used
_list_compare_func(gconstpointer a, gconstpointer b)
{
	return ( g_utf8_collate(g_app_info_get_name((GAppInfo*)a),
							g_app_info_get_name((GAppInfo*)b)) > 0 );
}

// Recherche d'une application par son nom dans la liste chaînée.
static gint __used
_list_find_func(gconstpointer a, gconstpointer b)
{
	if ( g_utf8_collate(g_app_info_get_name((GAppInfo *)a),
											(const gchar*)b) == 0 ) {
		return 0;
	}

	return -1;
}

/*
 * Dialogue 'À propos...' et fonctions relatives aux environnements de bureau.
 * HACK : les boutons sont munis d'icônes.
 */
GtkWidget*
_find_button_box(GtkWidget* parent)
{
	GtkWidget *widget = NULL;

	// Ne rechercher que parmi les widgets de type GTK_CONTAINER.
	GList *children = gtk_container_get_children(GTK_CONTAINER(parent));
	while ( children && GTK_IS_CONTAINER(children->data) ) {

		if ( GTK_IS_BUTTON_BOX(children->data) ) {
			widget = children->data;

			break;
		}

		widget = _find_button_box(children->data);

		children = children->next;
	}

	g_list_free(children);

	return widget;
}

GtkWidget*
_find_stack_switcher(GtkWidget* parent)
{
	GtkWidget *widget = NULL;

	// Ne rechercher que parmi les widgets de type GTK_CONTAINER.
	GList *children = gtk_container_get_children(GTK_CONTAINER(parent));
	while ( children && GTK_IS_CONTAINER(children->data) ) {

		if ( GTK_IS_STACK_SWITCHER(children->data) ) {
			widget = children->data;

			break;
		}

		widget = _find_stack_switcher(children->data);

		children = children->next;
	}

	g_list_free(children);

	return widget;
}

// Clic sur le menu 'Configuration'.
static void __used
activate_conf_menu_cb(GtkWidget __unused *menu_item, gpointer user_data)
{
	g_print("activate_conf_menu_cb\n");

	// Accès à la structure des données privées.
	ETechAppWindowPrivate *priv =
							etech_app_window_get_instance_private(user_data);

	/*
	 * Drapeau empêchant le fenêtre d'être fermée par le signal 'deactivate'
	 * du menu des catégories et de quitter l'application.
	 */
	priv->dialog_flag = TRUE;

	GtkWidget *od = etech_options_dialog_new(priv->cfg);
	gtk_window_set_position(GTK_WINDOW(od), GTK_WIN_POS_CENTER_ALWAYS);
	gtk_window_set_title(GTK_WINDOW(od), "MenuCat - Configuration");
	gtk_window_set_icon(GTK_WINDOW(od), etech_app_get_icon(priv->app));
	if ( GTK_IS_WIDGET(od) ) {

		// Boucle d'exécution du dialogue.
		gtk_dialog_run(GTK_DIALOG(od));

		// Destruction du dialogue et fin de la boucle.
		gtk_widget_destroy(od);
	}

	// Fermer la fenêtre (et donc quitter l'application).
	gtk_window_close(GTK_WINDOW(user_data));
}

static void __used
activate_about_menu_cb(GtkWidget __unused *menu_item, gpointer user_data)
{
	g_print("activate_about_menu_cb\n");

	// Accès à la structure des données privées.
	ETechAppWindowPrivate *priv =
							etech_app_window_get_instance_private(user_data);

	// Drapeau empêchant le fenêtre d'être fermée et de quitter l'application.
	priv->dialog_flag = TRUE;

	// Cette méthode permet une modalité contrôlée.
	GtkWidget *ad = gtk_about_dialog_new();

	// Version, programme, droits d'auteur, site internet.
	gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(ad), PACKAGE_VERSION);
	gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(ad), PACKAGE_NAME);
	gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(ad), COPYRIGHT_AUTHOR);
	gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(ad),
												"http://www.estereltech.com");
	gtk_about_dialog_set_website_label(GTK_ABOUT_DIALOG(ad),
												"http://www.estereltech.com");

	gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(ad),
												etech_app_get_logo(priv->app));

	// Auteur(s).
	const gchar *authors[] =
	{
		"Philippe Maréchal",
		"Autre personne",
		"Autre personne",
		"Autre personne",
		NULL
	};
	gtk_about_dialog_set_authors(GTK_ABOUT_DIALOG(ad), authors);

	// Documentation.
	const gchar *documenters[] =
	{
		"Philippe Maréchal",
		"Autre personne",
		"Autre personne",
		"Autre personne",
		NULL
	};
	gtk_about_dialog_set_documenters(GTK_ABOUT_DIALOG(ad), documenters);

	const gchar *artists[] =
	{
		"Philippe Maréchal",
		"Autre personne",
		"Autre personne",
		"Autre personne",
		NULL
	};
	gtk_about_dialog_set_artists(GTK_ABOUT_DIALOG(ad), artists);

	// Traducteurs, commentaires, licence etc.
	gtk_about_dialog_set_translator_credits(GTK_ABOUT_DIALOG(ad),
												"Traducteur #1\nTraducteur #2");
	const gchar *comments =
					"Menu d'applications de bureau conforme à Freedesktop.org.";
	gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(ad), comments);
	gtk_about_dialog_set_wrap_license(GTK_ABOUT_DIALOG(ad), TRUE);

	gtk_about_dialog_set_license(GTK_ABOUT_DIALOG(ad),
	"This program is free software; you can redistribute it and/or modify "
	"it under the terms of the GNU General Public License as published by "
	"the Free Software Foundation; either version 2 of the License, or "
	"(at your option) any later version.).");

	/*
	 * HACK !
	 * Retrouver les boutons (ou boutons radio) et leur affecter une
	 * icône en ressources.
	 * L'accès public à 'action_area' est dépréciée :
	 * gtk_dialog_get_action_area has been deprecated since version
	 * 3.12, il va falloir y accèder par la hiérachie des 'widgets'.
	 * Le bouton 'Fermer' envoie la réponse GTK_RESPONSE_DELETE_EVENT.
	 * Gnome place 3 boutons radio dans un GtkStackSwitcher dans la
	 * barre de titre du dialogue alors que XFCE place les boutons dans
	 * une GtkButtonBox au bas du dialogue.
	 */
	gboolean is_gnome_desktop = FALSE;
	const gchar *xcd = NULL;
	xcd = g_getenv("XDG_CURRENT_DESKTOP");
	if ( xcd ) {
		if ( g_strcmp0(xcd, "GNOME") == 0 ) {
			is_gnome_desktop = TRUE;
		}
	} else {
		g_warning("g_getenv(\"XDG_CURRENT_DESKTOP\" = NULL\n");
	}

	GtkWidget *btnbox = NULL, *stackswitcher = NULL;
	if ( is_gnome_desktop ) {
		stackswitcher = _find_stack_switcher(ad);
	} else {
		btnbox = _find_button_box(ad);
	}

	if ( stackswitcher ) {

	    GList *children = NULL;
	    children = gtk_container_get_children(GTK_CONTAINER(stackswitcher));
	    GList *btnchildren = NULL;
	    GtkWidget *w = NULL;

	    if ( GTK_IS_RADIO_BUTTON(G_OBJECT(g_list_nth_data(children, 0))) ) {

	        GtkWidget *btnabout = GTK_WIDGET(g_list_nth_data(children, 0));
	        gchar *about = NULL;
	        btnchildren = gtk_container_get_children(GTK_CONTAINER(btnabout));
	        w = GTK_WIDGET(g_list_nth_data(btnchildren, 0));
	        if ( w ) {
	            if ( GTK_IS_LABEL(w) ) {
	                about = g_strdup(gtk_label_get_text(GTK_LABEL(w)));
	            }
	        }
	        g_list_free(btnchildren);

	        gtk_button_set_always_show_image(GTK_BUTTON(btnabout), TRUE);
	        gtk_button_set_image(GTK_BUTTON(btnabout),
							gtk_image_new_from_resource("/icons/info.png"));
	        gtk_button_set_label(GTK_BUTTON(btnabout), about);
	        g_free(about);
	    }

	    if ( GTK_IS_RADIO_BUTTON(G_OBJECT(g_list_nth_data(children, 1))) ) {
	        GtkWidget *btncredits = GTK_WIDGET(g_list_nth_data(children, 1));
	        gchar *credits = NULL;
	        btnchildren = gtk_container_get_children(GTK_CONTAINER(btncredits));
	        w = GTK_WIDGET(g_list_nth_data(btnchildren, 0));
	        if ( w ) {
	            if ( GTK_IS_LABEL(w) ) {
	                credits = g_strdup(gtk_label_get_text(GTK_LABEL(w)));
	            }
	        }
	        g_list_free(btnchildren);

	        gtk_button_set_always_show_image(GTK_BUTTON(btncredits), TRUE);
	        gtk_button_set_image(GTK_BUTTON(btncredits),
							gtk_image_new_from_resource("/icons/cat_16.png"));
	        gtk_button_set_label(GTK_BUTTON(btncredits), credits);
	        g_free(credits);
	    }

	    if ( GTK_IS_RADIO_BUTTON(G_OBJECT(g_list_nth_data(children, 2))) ) {
	        GtkWidget *btnlicense = GTK_WIDGET(g_list_nth_data(children, 2));
	        gchar *license = NULL;
	        btnchildren = gtk_container_get_children(GTK_CONTAINER(btnlicense));
	        w = GTK_WIDGET(g_list_nth_data(btnchildren, 0));
	        if ( w ) {
	            if ( GTK_IS_LABEL(w) ) {
	                license = g_strdup(gtk_label_get_text(GTK_LABEL(w)));
	            }
	        }
	        g_list_free(btnchildren);

	        gtk_button_set_always_show_image(GTK_BUTTON(btnlicense), TRUE);
	        gtk_button_set_image(GTK_BUTTON(btnlicense),
							gtk_image_new_from_resource("/icons/cat_16.png"));
	        gtk_button_set_label(GTK_BUTTON(btnlicense), license);
	    }

		g_list_free(children);

	} else if ( btnbox ) {

		GList *children = NULL;
        children = gtk_container_get_children(GTK_CONTAINER(btnbox));
		if ( GTK_IS_TOGGLE_BUTTON(G_OBJECT(g_list_nth_data(children, 0))) ) {
			GtkWidget *btncredits = GTK_WIDGET(g_list_nth_data(children, 0));
			gtk_button_set_always_show_image(GTK_BUTTON(btncredits), TRUE);
			gtk_button_set_image(GTK_BUTTON(btncredits),
							gtk_image_new_from_resource("/icons/info.png"));
		}

		if ( GTK_IS_TOGGLE_BUTTON(G_OBJECT(g_list_nth_data(children, 1))) ) {
			GtkWidget *btnlicense = GTK_WIDGET(g_list_nth_data(children, 1));
			gtk_button_set_always_show_image(GTK_BUTTON(btnlicense), TRUE);
			gtk_button_set_image(GTK_BUTTON(btnlicense),
							gtk_image_new_from_resource("/icons/cat_16.png"));
			gtk_widget_set_margin_start(btnlicense, 4);
		}

		if ( GTK_IS_BUTTON(G_OBJECT(g_list_nth_data(children, 2))) ) {
			GtkWidget *btncancel = GTK_WIDGET(g_list_nth_data(children, 2));
			gtk_button_set_always_show_image(GTK_BUTTON(btncancel), TRUE);
			gtk_button_set_image(GTK_BUTTON(btncancel),
							gtk_image_new_from_resource("/icons/cancel.png"));
		}

		g_list_free(children);
    }

	// Parent, position et titre.
	gtk_window_set_transient_for(GTK_WINDOW(ad), GTK_WINDOW(user_data));
	gtk_window_set_position(GTK_WINDOW(ad), GTK_WIN_POS_CENTER_ALWAYS);
	gtk_window_set_title(GTK_WINDOW(ad), "À propos...");

	if ( GTK_IS_WIDGET(ad) ) {

		// Boucle d'exécution du dialogue.
		gtk_dialog_run(GTK_DIALOG(ad));

		// Destruction du dialogue et fin de la boucle.
		gtk_widget_destroy(ad);
	}

	// Fermer la fenêtre (et quitter l'application).
	gtk_window_close(GTK_WINDOW(user_data));
}

/*
 * Clic sur le menu surgissant et exécution de l'application sélectionnée.
 */
static void __used
activate_menu_cb(GtkWidget *menu_item, gpointer __unused user_data)
{
	// Accès à la structure des données privées.
	ETechAppWindowPrivate *priv =
							etech_app_window_get_instance_private(user_data);

	/*
	 * Texte de l'élément de menu (nom de l'application à exécuter).
	 * Voir 'etech_utils[h,c]'.
	 */
	const gchar *text = img_menu_item_get_text(menu_item);
	if ( ! text ) {
		g_print("! text\n");

		return;
	}

	// Retrouver l'application par le texte du menu.
	GList *found = g_list_find_custom(priv->apps, text, _list_find_func);
	if ( ! found ) {
		g_print("! found\n");

		return;
	}

	/*
	 * 'command' est la ligne de commande complète de l'application.
	 * Une nouvelle ligne de commande purgée des 'uris' (%U, %f etc.) est créée
	 * si nécessaire.
	 * 'cmd' pointe sur 'command' ou bien si 'ptr' pointe sur un caractère '%'
	 * trouvé dans 'command' le bloc mémoire 'cmd' est créé, la mémoire
	 * nécessaire est allouée sur la pile par la fonction 'alloca' et la chaîne
	 * extraite de 'command' est copiée dans 'cmd'.
	 */
	const gchar *command = NULL;
	command = g_app_info_get_commandline((GAppInfo*)found->data);
	if ( ! command ) {
		g_print("! command\n");

		return;
	}

	gchar *cmd = (gchar*)command, *ptr = NULL;

	// Si '%' est trouvé dans la ligne de commande.
	if ( (ptr = strchr(command, 0x25)) ) {
		size_t slen = (ptr - command);
		cmd = (gchar*)g_alloca(slen + 1);
		memcpy(cmd, command, slen);
		cmd[slen] = 0x00;
	}

	/*
	 * Recherche de /etc/alternatives/x-terminal-emulator qui est l'émulateur
	 * de Terminal par défaut sur la distribution Debian et ses dérivées.
	 * Note : pour Gnome la commande est : gnome-terminal -- <commande>.
	 */
	gboolean debian_default_terminal_emulator = FALSE;
	gint ret = g_access("/etc/alternatives/x-terminal-emulator", F_OK | R_OK);
	if ( ret == 0 ) {
		debian_default_terminal_emulator = TRUE;
		g_print("Trouvé > /etc/alternatives/x-terminal-emulator\n");
	}

	/*
	 * Si 'Terminal = true' dans le fichier *.desktop - l'application doit être
	 * exécutée par un terminal (ex. htop).
	 */
	gchar final_cmd[CMD_LEN + 1] = {0};
	if ( g_desktop_app_info_get_boolean((GDesktopAppInfo*)found->data,
																"Terminal") ) {
		// Si le terminal par défaut est indiqué (ex. Debian et dérivées).
		if ( debian_default_terminal_emulator ) {
			g_snprintf(final_cmd, CMD_LEN, "x-terminal-emulator -e %s", cmd);
		} else {

			// Utiliser la clé de la configuration si elle existe.
			if ( priv->term_cmd ) {
				g_snprintf(final_cmd, CMD_LEN, "%s %s", priv->term_cmd, cmd);
			} else {

				// Ne pas exécuter la commande.
				g_warning("Un émulateur de terminal est nécessaire pour "
						"exécuter la commande !\n");
				return;
			}
		}
	} else {
		strcpy(final_cmd, cmd);
	}

#if 1
	g_print("command = %s - cmd = %s\n", command, final_cmd);
#endif

	GError *err = NULL;

#if 1
	// Solution asynchrone, la plus 'propre' selon mes tests.
    if ( ! g_spawn_command_line_async(final_cmd, &err) ) {
        g_printerr("Error: %s\n", err->message);
        g_error_free(err);
    }
#endif
#if 0
    // Solution synchrone avec contrôle du code de sortie.
	gint exit_status;
    gboolean success = FALSE;
	success = g_spawn_command_line_sync(final_cmd,
										NULL,
										NULL,
										&exit_status,
										&err);
    g_print("\nsuccess = %d - exit_status = %d\n\n", success, exit_status);
#endif

	// La fenêtre est fermée par le gestionnaire 'deactivate_menu_cats_cb'.
}

static GtkWidget* __used
_create_cats_menu(ETechAppWindow *win)
{
	// Accès à la structure des données privées.
	ETechAppWindowPrivate *priv = etech_app_window_get_instance_private(win);

	// Noms des icônes correspondant aux catégories dans un thème d'icônes.
	const gchar *icon_names[NB_CAT] = {
		"applications-accessories",
		"applications-office",
		"applications-science",
		"applications-graphics",
		"applications-internet",
		"applications-games",
		"applications-multimedia",
		"preferences-desktop",
		"applications-development",
		"applications-system"
	};

	priv->cats_menu = gtk_menu_new();

	// Réserver ou non l'espace nécessaire pour une image ou une case à cocher.
	gtk_menu_set_reserve_toggle_size(GTK_MENU(priv->cats_menu), FALSE);

	GtkWidget *img;
	guint i = 0;
	GList *lmi = NULL;

	for (; i < G_N_ELEMENTS(icon_names); ++i) {

		if ( ! GTK_IS_WIDGET(priv->menus[i]) ) {
			g_print("! GTK_IS_WIDGET(priv->menus[%d])\n", i);
		}

		/*
		 * Si un menu ne contient aucun élément, ne pas créer de catégorie pour
		 * lui (par ex. la catégorie 'Éducation').
		 */
		lmi = gtk_container_get_children(GTK_CONTAINER(priv->menus[i]));
		if ( ! lmi ) {
			g_warning("Le menu %s est vide\n", CATEGORY[i]);
			g_list_free(lmi);

			continue;
		}

		g_list_free(lmi);

		// Icône du thème en cours pour la catégorie concernée.
		img = gtk_image_new_from_icon_name(icon_names[i], GTK_ICON_SIZE_MENU);
		GtkWidget *mi = create_img_menu_item(img, CATEGORY[i]);
		if ( ! mi ) {
			g_print("! mi\n");

			return NULL;
		}

		//  Ajout de l'élément au menu.
		gtk_menu_shell_append(GTK_MENU_SHELL(priv->cats_menu), mi);

		/*
		 * Joindre son sous-menu (déjà créé par 'create_all_menus) au menu
		 * de la catégorie.
		 */
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(mi), priv->menus[i]);
    }

	// Séparateur et menu pour la configuration.
    GtkWidget *sep = gtk_separator_menu_item_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(priv->cats_menu), sep);

	// Menu 'Configuration' (icône "preferences-desktop").
    img = gtk_image_new_from_resource("/icons/cat_16.png");
	GtkWidget *miconf = create_img_menu_item(img, "Configuration");
	if ( ! miconf ) {
		g_print("! miconf\n");

		return NULL;;
	}
    gtk_menu_shell_append(GTK_MENU_SHELL(priv->cats_menu), miconf);
    gtk_widget_set_tooltip_text(miconf, "Configuration de MenuCat");

    // Gestionnaire de signal pour le menu 'Configuration'.
	g_signal_connect(G_OBJECT(miconf),
					"activate",
					G_CALLBACK(activate_conf_menu_cb),
					win);

	// Séparateur avant le menu 'À propos...'.
	sep = gtk_separator_menu_item_new();
    gtk_menu_shell_append(GTK_MENU_SHELL(priv->cats_menu), sep);
    img = gtk_image_new_from_resource("/icons/info.png");
	GtkWidget *miabout = create_img_menu_item(img, "À propos...");
	if ( ! miabout ) {
		g_print("! miabout\n");

		return NULL;;
	}
    gtk_menu_shell_append(GTK_MENU_SHELL(priv->cats_menu), miabout);
    gtk_widget_set_tooltip_text(miabout, "À propos de MenuCat");

    // Gestionnaire de signal pour le menu 'À propos...'.
	g_signal_connect(G_OBJECT(miabout),
					"activate",
					G_CALLBACK(activate_about_menu_cb),
					win);

	return priv->cats_menu;
}

// Création d'un menu pour les applications d'une catégorie.
static gboolean
_create_menu_for_cat(guint cat_id, ETechAppWindow *win)
{
	// Accès à la structure des données privées
	ETechAppWindowPrivate *priv = etech_app_window_get_instance_private(win);

	if ( ! priv->apps || g_list_length(priv->apps) == 0 ) {
		return FALSE;
	}

	// Créer le menu à l'indice 'cat_id' du tableau 'menus'.
	priv->menus[cat_id] = gtk_menu_new();
	if ( ! priv->menus[cat_id] ) {
		g_print("! priv->menus[cat_id]\n");

		return FALSE;
	}

	// Réserver ou non l'espace nécessaire pour une image ou une case à cocher.
	gtk_menu_set_reserve_toggle_size(GTK_MENU(priv->menus[cat_id]), FALSE);

	// Boucle pour l'ajout au menu d'éléments avec textes et icônes.
	for (GList *l = priv->apps; l != NULL; l = l->next) {

		if ( (GAppInfo*)l->data &&
			! g_desktop_app_info_get_nodisplay((GDesktopAppInfo*)l->data) ) {

			// Chaîne des catégories de l'application.
			const char *cats =
				g_desktop_app_info_get_categories((GDesktopAppInfo*)l->data);

			if ( ! cats ) {

				// Cette application ne fournit aucune catégorie.
				fprintf(stderr, "%s - Erreur : ! cats !\n",
									g_app_info_get_name((GAppInfo*)l->data));

				continue;
			}

			// Si l'application appartient à la catégorie demandée (icat).
			gboolean bcat = FALSE;
			switch(cat_id)
			{
			case CAT_DEVELOPMENT:
				if ( strstr(cats, "Development") ) {
					bcat = TRUE;
				}
				break;

			case CAT_SETTINGS:
				if ( strstr(cats, "Settings") ) {
					bcat = TRUE;
				}
				break;

			case CAT_NETWORK:
				if ( strstr(cats, "Network") ) {
					bcat = TRUE;
				}
				break;

			case CAT_SYSTEM:
				if ( strstr(cats, "System") ) {
					bcat = TRUE;
				}
				break;

			case CAT_GAME:
				if ( strstr(cats, "Game") ) {
					bcat = TRUE;
				}
				break;

			case CAT_UTILITY:
				if ( strstr(cats, "Utility") ) {
					bcat = TRUE;
				}
				break;

			case CAT_GRAPHICS:
				if ( strstr(cats, "Graphics") ) {
					bcat = TRUE;
				}
				break;

			case CAT_AUDIOVIDEO:
				if ( strstr(cats, "AudioVideo") ) {
					bcat = TRUE;
				}
				break;

			case CAT_OFFICE:
				if ( strstr(cats, "Office") ) {
					bcat = TRUE;
				}
				break;

			case CAT_EDUCATION:
				if ( strstr(cats, "Education") ) {
					bcat = TRUE;
				}
				break;

			default:
				break;
			}

			if ( bcat == TRUE ) {

				// Création de l'image à partir de l'icône de l'application.
				GtkWidget *img = gtk_image_new_from_gicon(
								g_app_info_get_icon((GAppInfo*)l->data),
								GTK_ICON_SIZE_BUTTON);

				// Garantir les dimensions appropriées de l'image (16px).
				gtk_image_set_pixel_size(GTK_IMAGE(img), IMG_SIZE);

				// Création d'un widget 'élément de menu' avec image et texte.
				GtkWidget *menu_item = create_img_menu_item(img,
							(gchar*)g_app_info_get_name((GAppInfo*)l->data));
				if ( menu_item == NULL ) {
					g_print("! create_menu_item\n");
				}

				// Connexion d'un signal pour l'événement 'clic sur le menu'.
				g_signal_connect(G_OBJECT(menu_item),
									"activate",
									G_CALLBACK(activate_menu_cb),
									win);

				//  Ajout de l'élément au menu.
				gtk_menu_shell_append(GTK_MENU_SHELL(priv->menus[cat_id]),
															menu_item);
			}
		}
	}

	// Rendre tous les widgets (de type GtkMenuItem) du menu visibles.
	gtk_widget_show_all(priv->menus[cat_id]);

	return TRUE;
}

static gboolean __used
_create_all_menus(ETechAppWindow *win)
{
	for (guint i = 0; i < NB_CAT; ++i) {

		/*
		 * Créer le menu pour la catégorie.
		 * En cas d'erreur, passer à la catégorie suivante.
		 */
		if ( ! _create_menu_for_cat(i, win) ) {
			g_print("! create_menu_for_cat\n");

			continue;
		}
	}

	return TRUE;
}

static void
deactivate_menu_cats_cb(GtkMenuShell __unused *menushell,
						gpointer __unused user_data)
{
	// Accès à la structure des données privées
	ETechAppWindow *win = (ETechAppWindow*)user_data;
    ETechAppWindowPrivate *priv = etech_app_window_get_instance_private(win);

	g_print("deactivate_menu_cats_cb\n");

	/*
	 * Si le drapeau empêchant la fenêtre d'être fermée et de quitter
	 * l'application (dialogues modaux 'À propos' et de configuration) est
	 * activé, ne pas fermer et quitter, le gestionnaire de signal du dialogue
	 * concerné s'en chargera.
	 */
	if ( ! priv->dialog_flag ) {
		gtk_window_close(GTK_WINDOW(win));
	}
}

static void
popped_up_menu_cats_cb(GtkMenu __unused *menu,
					gpointer __unused flipped_rect,
					gpointer __unused final_rect,
					gboolean __unused flipped_x,
					gboolean __unused flipped_y,
					gpointer __unused user_data)
{
	// Accès à la structure des données privées
	ETechAppWindow *win = (ETechAppWindow*)user_data;
    ETechAppWindowPrivate *priv = etech_app_window_get_instance_private(win);

    priv->popped_up = TRUE;
}

/*
 * La fenêtre est composée.
 * Il est possible de rendre ses dimensions définitives mais cela n'a pas
 * d'intérêt ici.
 */
static void
etech_app_window_realize_cb(GtkWidget *widget, gpointer __unused user_data)
{
    gtk_window_set_resizable(GTK_WINDOW(widget), TRUE);
}

// Destructeur de la fenêtre (pas de l'objet).
static void
etech_app_window_destroy_cb(GtkWidget __unused *window,
							gpointer __unused user_data)
{
	g_print("etech_app_window_destroy_cb\n");
}

static
void window_show_cb(GtkWidget *win, gpointer __unused user_data)
{
	g_print("4 - window_show_cb\n");

	// Accès à la structure des données privées et initialisation de bactive.
    ETechAppWindowPrivate *priv =
				etech_app_window_get_instance_private(ETECH_APP_WINDOW(win));

	if ( ! priv->popped_up ) {

		// Affichage du menu dans l'angle nord-ouest de la fenêtre.
		gtk_menu_popup_at_widget(GTK_MENU(priv->cats_menu),
								win,
								GDK_GRAVITY_NORTH_WEST,
								GDK_GRAVITY_NORTH_EAST,
								NULL);

		priv->popped_up = TRUE;
	}
}

// Fenêtre principale de l'application.
static void
etech_app_window_init(ETechAppWindow *win)
{
    // Accès à la structure des données privées et initialisation de bactive.
    ETechAppWindowPrivate *priv = etech_app_window_get_instance_private(win);

    // Fenêtre principale, titre, dimensions, position et icône.
    gtk_window_set_default_size(GTK_WINDOW(win), 1, 1);
	gtk_window_set_title(GTK_WINDOW(win), "MenuCat - Applications");
    gtk_window_set_decorated(GTK_WINDOW(win), 	FALSE);
    // gtk_window_set_keep_above(GTK_WINDOW(win), 	TRUE);

    /*
     * La fenêtre n'apparaîtra pas dans la barre des tâches ni dans un changeur
     * d'espace de travail.
     */
    gtk_window_set_skip_taskbar_hint(GTK_WINDOW(win), 	TRUE);
    gtk_window_set_skip_pager_hint(GTK_WINDOW(win), 	TRUE);

	// Liste chaînée des applications
    priv->apps = g_app_info_get_all();

    // Tri des applications - ordre alphabétique - par nom.
	priv->apps = g_list_sort(priv->apps, _list_compare_func);
	if ( ! priv->apps ) {
		g_print("Erreur lors du tri !\n");
	}

	// Création des menus des catégories.
	if ( ! _create_all_menus(win) ) {
		g_print("! create_all_menus\n");
    }

    /*
     * Note : le menu 'priv->cats_menu' est 'flottant' et doit être attaché à
     * la fenêtre principale de l'application. Tous les autres menus du
     * tableau 'priv->menus' sont des sous-menus de 'priv->cats_menu' sauf ceux
     * qui sont vides qui seront supprimés.
     */
    _create_cats_menu(win);
    if ( ! priv->cats_menu ) {
		g_print("! cats_menu\n");
    }
    gtk_widget_show_all(priv->cats_menu);
    gtk_menu_attach_to_widget(GTK_MENU(priv->cats_menu), GTK_WIDGET(win), NULL);

	// Initialisation des deux drapeaux.
    priv->dialog_flag 	= FALSE;
    priv->popped_up 	= FALSE;

    // Configuration.
	priv->cfg = config_new();
	if ( ! priv->cfg ) {
		g_print("! priv->cfg\n");
	}

	// Coordonnées fournies par la configuration.
	priv->left_coord = 4;
	priv->top_coord  = 100;
	if ( config_load(priv->cfg) ) {
		if ( ! config_get_menu_coords(priv->cfg,
									&priv->left_coord,
									&priv->top_coord) ) {
			g_print("! config_get_menu_coords\n");
		}

		priv->term_cmd = config_get_terminal_command(priv->cfg);
		if ( priv->term_cmd == NULL ) {
			g_print("priv->term-cmd == NULL\n");
		}
	}

    /*
     * Gestionnaire de signal pour la désactivation du menu principal des
     * catégories. C'est ce signal qui fermera l'application.
     */
    g_signal_connect(priv->cats_menu,
					"deactivate",
					G_CALLBACK(deactivate_menu_cats_cb),
					win);

	g_signal_connect(priv->cats_menu,
					"popped-up",
					G_CALLBACK(popped_up_menu_cats_cb),
					win);
	/*
	 * Connexion de signaux aux gestionnaires d'événements.
	 */
	g_signal_connect(G_OBJECT(win), "realize",
    							G_CALLBACK(etech_app_window_realize_cb), NULL);
    g_signal_connect(G_OBJECT(win), "destroy",
								G_CALLBACK(etech_app_window_destroy_cb), NULL);

	g_signal_connect(G_OBJECT(win), "show", G_CALLBACK(window_show_cb), NULL);

	g_print("1 - etech_app_window_init\n");
}

static void
etech_app_window_constructed(GObject *object)
{
	G_OBJECT_CLASS(etech_app_window_parent_class)->constructed(object);
	g_print("2 - etech_app_window_constructed\n");

	// Accès à la structure des données privées.
    ETechAppWindowPrivate *priv =
		etech_app_window_get_instance_private(ETECH_APP_WINDOW(object));

	/*
	 * Positionnement sur l'écran aux coordonnées de la configuration.
	 * Aussitôt que la fenêtre vide s'affiche, le signal 'focus-in' est émis et
	 * déclenche l'apparition du menu aux coordonnées de la fenêtre invisible.
	 */
	gtk_window_move(GTK_WINDOW(object), priv->left_coord, priv->top_coord);
}

/*
 * Cette fonction peut être appelée plusieurs fois donc faire attention lors de
 * la libération des ressources.
 */
static void
etech_app_window_dispose(GObject *object)
{
	static gint times = 0;
	g_print("etech_app_window_dispose - %d\n", (times + 1));
	times++;

	// Accès à la structure des données privées.
	ETechAppWindow *win = (ETechAppWindow*)object;
    ETechAppWindowPrivate *priv = etech_app_window_get_instance_private(win);

    /*
     * Libération des ressources.
     * Destruction de la configuration.
     * Destruction de la liste chaînée et des menus.
     * La liste contiendra au moins une application et tous les menus sont
     * construits même s'ils peuvent rester vides d'éléments.
     * La configuration est détruite par l'application.
     */
    if ( priv->cfg ) {
		g_print("config_free(priv->cfg)\n");
		config_free(priv->cfg);
		priv->cfg = NULL;
    }

    if ( priv->apps ) {
		g_print("g_list_free_full(priv->apps, g_object_unref)\n");
		g_list_free_full(priv->apps, g_object_unref);
		priv->apps = NULL;
	}

	if ( GTK_IS_WIDGET(priv->cats_menu) ) {
		g_print("GTK_IS_WIDGET(priv->cats_menu)\n");
		gtk_widget_destroy(priv->cats_menu);
	}

	for (guint i = 0; i < NB_CAT; ++i) {
		if ( GTK_IS_WIDGET(priv->menus[i]) ) {
			g_print("GTK_IS_WIDGET(priv->menus[%d])\n", i);

			/*
			 * Pour les éventuels menus du tableau 'priv->menus' qui sont vides,
			 * inutilisés et n'appartiennent à aucun Widget parent et qui ont
			 * le drapeau 'floating' activé.
		     */
			if ( g_object_is_floating(priv->menus[i]) ) {
				g_print("g_object_is_floating(priv->menus[%d])\n", i);
				g_object_ref_sink(priv->menus[i]);
				g_object_unref(priv->menus[i]);
			} else {
				gtk_widget_destroy(priv->menus[i]);
			}
		}
	}

    G_OBJECT_CLASS(etech_app_window_parent_class)->dispose(object);
}

static void
etech_app_window_finalize(GObject *object)
{
	g_print("etech_app_window_finalize\n");

    G_OBJECT_CLASS(etech_app_window_parent_class)->finalize(object);
}

static void
etech_app_window_class_init(ETechAppWindowClass *class)
{
	G_OBJECT_CLASS(class)->constructed 	= etech_app_window_constructed;
    G_OBJECT_CLASS(class)->dispose 		= etech_app_window_dispose;
    G_OBJECT_CLASS(class)->finalize 	= etech_app_window_finalize;
}


//// Public.


ETechAppWindow*
etech_app_window_new(ETechApp *app)
{
    ETechAppWindow *win = g_object_new(ETECH_APP_WINDOW_TYPE,
										"application",
										app,
										NULL);

	// Accès à la structure des données privées.
    ETechAppWindowPrivate *priv = etech_app_window_get_instance_private(win);

	// Initilisation.
    priv->app = app;

    g_print("3 - etech_app_window_new\n");

    return win;
}

